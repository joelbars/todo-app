resource "aws_instance" "app" {
  ami = data.aws_ami.image.id
  instance_type = var.ec2_instance_type
  key_name = var.ec2_keypair
  count = var.ec2_count
  tags = {
    Name = var.ec2_tags
  }
  subnet_id = aws_subnet.public_subnet.id
  vpc_security_group_ids = [aws_security_group.app.id]
  associate_public_ip_address = true
  iam_instance_profile = aws_iam_instance_profile.app_profile.name
}

output "instance_ip_addr" {
  value       = aws_instance.app.*.private_ip
  description = "The private IP address of the main server instance."
}

output "instance_ips" {
  value = aws_instance.app.*.public_ip
}