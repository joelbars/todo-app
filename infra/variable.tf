variable "ec2_region" {
  default = "us-east-1"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_bucket" {
  default = "todo-app-backend"
}

variable "ec2_keypair" {
  default = "personal-windows"
}

variable "ec2_tags" {
  default = "webserver"
}

variable "ec2_count" {
  default = "1"
}

variable "AMI_NAME" {
  type = string
}