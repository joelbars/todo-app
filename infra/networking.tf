resource "aws_subnet" "private_subnet" {
  cidr_block = "192.168.10.0/24"
  vpc_id = aws_vpc.vpc.id
  map_public_ip_on_launch = false
  availability_zone = "us-east-1b"
}

resource "aws_subnet" "public_subnet" {
  cidr_block= "192.168.1.0/24"
  vpc_id= aws_vpc.vpc.id
  map_public_ip_on_launch = true
  availability_zone = "us-east-1a"
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "rt_association" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.route_table.id
}
