const mongoose = require('mongoose');

const options = {
  autoReconnect : true,
  reconnectInterval: 60000,
  reconnectTries: 99,
};

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, options);

module.exports = { mongoose };